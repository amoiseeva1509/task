package consulting;

import au.com.bytecode.opencsv.CSVReader;
import org.eclipse.jetty.util.ConcurrentHashSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class Parser {

    private static final Logger LOGGER = LoggerFactory.getLogger(Parser.class);
    private ConcurrentHashMap<String, ConcurrentHashSet<String>> uniqValues = new ConcurrentHashMap<>();

    public void processFile(String [] files) {
        Arrays.stream(files)
                .parallel()
                .forEach(file -> read(file));
        uniqValues.entrySet()
                .stream()
                .filter(e -> !e.getKey().equals(""))
                .forEach(e -> write(e.getKey(), e.getValue()));
    }

    private void write(String fileName, ConcurrentHashSet<String> values) {
        File file = new File(fileName + ".txt");
        try (FileWriter writer = new FileWriter(file)) {
            file.createNewFile();
            writer.write(values.stream().map(s -> s + ";").collect(Collectors.joining()));
        } catch (IOException e) {
            LOGGER.warn("Can not write into {}", fileName);
            return;
        }
    }

    private void read(String fileName) {
        try (CSVReader reader = new CSVReader(new FileReader(fileName), ';')) {
            List<String[]> records = reader.readAll();
            Optional.ofNullable(records.get(0)).ifPresent(headers -> {
                fillOutKeys(headers);
                fillOutValues(records, headers);
            });
        } catch (IOException e) {
            LOGGER.warn("Can not read {}", fileName);
            return;
        }
    }

    private void fillOutKeys(String [] headers) {
        for (int i = 0; i < headers.length; i++) {
            uniqValues.putIfAbsent(headers[i], new ConcurrentHashSet<>());
        }
    }

    private void fillOutValues(List<String[]> records, String [] headers) {
        for (int i = 0; i < headers.length; i++) {
            for(int j = 1; j < records.size(); j++) {
                uniqValues.get(headers[i]).add(records.get(j)[i]);
            }
        }
    }

}
